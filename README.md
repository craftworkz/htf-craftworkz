# Craftworkz: IoT & AI challenge
The first 198 human beings landed on Mars. Conditions are bad. Oxygen and water run low. A meteorite storm is coming. There’s an unknown disease spreading fast, they think it’s nanobots. The AI indicates 12 hours left before the pressure tanks implode.
Aliens do not want to colonize us, they just want human’s going back to earth and protect their territory. Automate the security fence of connected cameras an computer vision by a platform able to detect your own spoken commando’s.

Let's build this solution step by step.

## Challenge 1:
Alright, let's start by exploring the capabilities of Watson.
### Watson Developer Cloud
IBM Watson (https://www.ibm.com/watson/) is a cognitive system. The cool part is, that is has APIs available for developers. Developers with little or no knowledge of artificial intelligence can use these powerful tools in their applications.
To get an idea of Watson's capabilities, start by exploring all de different services that Watson offers: https://www.ibm.com/watson/developercloud/services-catalog.html

### Watson Visual Recognition
We are especially interested in Visual Recognition (https://www.ibm.com/watson/developercloud/visual-recognition.html) for this challenge.

Visual Recognition allows users to understand the contents of an image or video frame, answering the question: “What is in this image?” Submit an image, and the service returns scores for relevant classifiers representing things such as objects, events and settings.

We want to send an image to the classifier and see the result. The result will determine whether or not there was an alien in the image. Explore the following SDK:
https://github.com/watson-developer-cloud/node-sdk
This is the official Watson SDK. With this SDK, you can interact with all the different services. For this challenge, take a look at the Visual Recognition section. If you're more comfortable with Python or Java code, there are SDK's available for these languages as well.

### Writing the code
Let's get our hands dirty. Below are three different images. One of a dog, two of an alien. Write some basic code where you can upload an image from your file system OR enter an image URL (you can choose between uploading from file system or entering a URL) and the system sends them to Watson Visual Recognition and prints the results. You'll find all the info you need here: https://www.ibm.com/watson/developercloud/visual-recognition/api/v3/#classify_an_image

![image1.jpg](https://bitbucket.org/repo/9E6oEk/images/474599143-image1.jpg)
![image2.jpg](https://bitbucket.org/repo/9E6oEk/images/632489907-image2.jpg)
![image4.jpg](https://bitbucket.org/repo/9E6oEk/images/2633724166-image4.jpg)

The api key you need is this one:
```
{
   "api_key": "243ed19843348fcdb81d320897e742f027887ab3"
}
```
You can use this API key to connect to the Visual Recognition service. This service contains one classifier that only knows two classes: 'alien' and 'normal'. Classify the images by sending them to this classifier. You can either download the images to your file system and let your program read them from your file system and upload them to Watson or you can send the image URL's to Watson (check the documentation!). Here are the URL's:
https://bitbucket-assetroot.s3.amazonaws.com/repository/9E6oEk/474599143-image1.jpg?Signature=0hxNPHe%2FmhCj2qAT3%2Fx%2B7RJrAYk%3D&Expires=1481180975&AWSAccessKeyId=AKIAIVFPT2YJYYZY3H4A&versionId=cVbEVdXi85zv8GDxWUW6wnWkKTGI8A6s

https://bitbucket-assetroot.s3.amazonaws.com/repository/9E6oEk/632489907-image2.jpg?Signature=ZN39LBc7n4PTL09mF0FXSngKis4%3D&Expires=1481181118&AWSAccessKeyId=AKIAIVFPT2YJYYZY3H4A&versionId=clIofJKnrYwxbyCuDVyXxHH20nk_nKCe

https://bitbucket-assetroot.s3.amazonaws.com/repository/9E6oEk/2633724166-image4.jpg?Signature=BSxSWsD6oWX6C0ZMp8piQ2degww%3D&Expires=1481181146&AWSAccessKeyId=AKIAIVFPT2YJYYZY3H4A&versionId=O75sD8GJpXsF0uyKqdcAGXSS5X46HMNv


Try to understand the SDK documentation to determine the other parameters. Can you find the right classifier_id? If not, ask for help! Also, try playing with the threshold parameter.
If everything works well, you should see in the output that Watson classifies the last two images as aliens (highest confidence for the alien class). This is pretty cool, don't you think? Now of course we need to automate this process and use images from the surveillance camera. So head on to challenge 2!

## Challenge 2:
So the Watson service is able to distinguish pictures with aliens from other pictures. Have you seen our surveillance camera yet? This is the camera that we will use to detect aliens that are invading our colony! The camera takes a picture every two seconds and uploads it to a server. To get access to latest six pictures, we provided a REST API.

The API documentation can be found at: https://hackthefuture-api.eu-gb.mybluemix.net/explorer/

You will see that there is one GETTER that returns a JSON object with one element 'images' which is a list of public URL's that can be sent to Watson. This list of images changes every two seconds. There are always around 6 images cached on the server. The name of the image contains a number. The image with the highest number is the most recent image.

The goal of this challenge is to build an automated program that polls the images from the server, through the API, uploads these images to the Watson Visual Recognition service and, if the service detects an alien, automatically tweets a message to your personal Twitter profile. The message should contain @craftworkz_co, #<YOUR TEAM NAME>, #htf2016 and a cool, creative message to warn Twitter an alien has been detected. Note that you can reuse the code from challenge 1 to classify an image through Watson's Visual Recognition service. If you're done, you can assume your colony is safe. Or can you? Let's move on to challenge 3 to make the surveillance system more robust!

## Challenge 3: controlling the camera
Oh no!
It looks like the aliens are trying to avoid detection!
We can only see aliens that are right in front of the camera. We need to be able to turn the camera to explore the surroundings.

We placed the camera on a platform that is able to turn 360 degrees. Use Watson Internet of Things to send commands to this platform to turn left or right.
### Watson Internet of Things
Watson IoT is a service on the IBM Bluemix platform used to connect and communicate with internet-connected devices. More info: https://www.ibm.com/cloud-computing/bluemix/internet-of-things
Our setup looks as follows:

![N|Solid](http://i.imgur.com/asZ3fML.png)
We connected the platform and configured IBM Watson IoT Platform. Now its up to you to send the commands using the REST APIs. Feel free to use any programming language. Look up in the documentation how to publish commands. For you NodeJS lovers out there, this package can be very helpful... https://www.npmjs.com/package/ibmiotf

*NOTE: You will be an application and not a client. The client is the platform, the application is the one sending the commands.*

### Configuration
You will probably need the following configuration somewhere along the way:
```
"org" : "5q3wxj",
"id" : "e3dd3522-e94e-4e49-9d44-a8391f6fe2cb",
"domain": "internetofthings.ibmcloud.com",
"auth-key" : "CHECK YOUR KEY BELOW",
"auth-token" : "CHECK YOUR TOKEN BELOW",
"deviceType": "craftworkz-platform",
"deviceId": "craftworkz123",
"commandName": "command"
```

#### Ex-Exomars Dev Team
```
Key: a-5q3wxj-4toruxzxcx
Token: ncsqiUk_reB-OOqUCg
```

#### Hackaholics
```
Key: a-5q3wxj-f1qpgl0wq5
Token: )5QDIiqTL&Yi@uA7+&
```

#### Humans of Internet
```
Key: a-5q3wxj-af5yc6puwc
Token: UXEcO!hfD1okt8m4pz
```

#### Mars Invaders
```
Key: a-5q3wxj-czuvvazsno
Token: Op6FHXN@SB(b_e7LTs
```

#### Martian Seekers
```
Key: a-5q3wxj-vrqv8wot9o
Token: YlYl-Q?a54-Hz-2Ew6
```

#### Martian Seekers 2
```
Key: a-5q3wxj-medbwdpnha
Token: -Xc2-ouAVGqllPW8+E
```

#### Pigs In Space
```
Key: a-5q3wxj-5zdejcmr8v
Token: yj2qTnhVrvhFI)J(w@
```

#### Run, you clever code!
```
Key: a-5q3wxj-ug8zacltpu
Token: jBbao6tI?L*vp23XAH
```

#### SV_cheats 1
```
Key: a-5q3wxj-9fapsaywdb
Token: sXPf+gSHd2arWLw2rO
```

#### Team PEBKAC
```
Key: a-5q3wxj-xivuolt3uv
Token: u@Yeg6RsDZ2XJD)b9i
```

#### Textual Harassment
```
Key: a-5q3wxj-r8q3tvoqvs
Token: k-I)b6Psbo9Ndb92wI
```

#### USS Enterprise
```
Key: a-5q3wxj-lz6kymgm53
Token: ZdSvD)Xk!pa&h)QGWl
```

#### We Don't C#
```
Key: a-5q3wxj-nycm4tryof
Token: 3R2DZlJ5k-ca4WBPeP
```

#### Tom & Jerry
```
Key: a-5q3wxj-jcnwddofix
Token: fphOnG*ILrSzovsEV7
```

A command you will have to publish needs to have the following format:
```
var data = {
    'direction' : 'right'
};
```
Once you see the platform turning left/right, you know it works!
### Making it (a bit more) user friendly
Of course you don't want to start your code in the terminal every time you want to turn the camera left or right. Make it more user friendly!

#### REST API
Create an API on top of the code you just wrote. The following result is desirable, but feel free to choose your own approach:
`POST /api/command` with the following body:
```
{
    "command": "right"
}
```
A great framework to achieve this, is ExpressJS: https://expressjs.com

#### Web interface
Last but not least, create a (very) simple web interface which talks to the REST API we just created. For example: show 2 buttons, left and right. If you click on them, send a `POST` to the endpoint we talked about. If that is the case, the code behind the endpoint should publish the command on the MQTT queue to IBM Watson IoT and then all the way to the camera platform.

### See the magic happen
If you reached this section of the challenge, stand up, jump around and call us to come and witness the magic!

## Challenge 4:
Alright, we built a very robust surveillance system. Aliens do not get a chance to invade our colony anymore. But let's make it even cooler. What we want for this challenge is a natural language classifier. Such a classifier labels sentences with a specific class. Let's say the two possible classes are 'left' and 'right'. The sentence "Hi there! Could you maybe please turn a little bit to the left?" will classify to 'left'. There are two ways to build this. You can choose either one, but bonus points if you use the Watson approach ;-)
### A simple natural language classifier
You can build a very simple classifier. Such a classifier looks at the sentences, looks at the different words in the sentence and if the word 'left' appears in the sentence, it classifies the sentence as 'left'. If the word 'right' appears in the sentence, the classifier classifies the sentence as 'right'.
### Watson Natural Language Classifier
So, as you could guess, Watson has a Natural Language Classifier service as well. Train this classifier with a few sentences that label to 'left' and sentences that label to 'right'. So this is the first time you will need to create and train a Watson service yourself. Remember that we trained the Visual Recognition service for you. This time, it's all up to you! The documentation for the Watson Natural Language Classifier can be found here: https://www.ibm.com/watson/developercloud/nl-classifier.html
#### Create an IBM Bluemix account
Go to http://console.eu-gb.bluemix.net to create a new Bluemix account. The first 30 days you try out Bluemix are completely free, so you can create and use the services you need.

### Frontend
Provide a little text field in the frontend where you can input your sentence, this will get classified by the classifier and the right command will be sent to Watson IoT to turn the camera.

## Challenge 5:
Challenge 4 did seem a little bit useless perhaps. Clicking on a button is faster and easier than entering a sentence. But what if you could speak to the system with your voice? For this challenge we will use Watson Speech-to-Text. Provide a button in the frontend to record a message, send it to Watson, get the text from the speech and connect this text to your code in challenge 4.

Wow, that's it! You did a great job! Thank you for helping us keeping our colony safe.
